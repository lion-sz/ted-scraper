from scrapy import Spider, Request
import pandas as pd
from typing import List, Dict
from pathlib import Path
import json
from scrapy.exceptions import IgnoreRequest
import sys
import configparser

from output_file import OutputFile


class TEDSpider(Spider):
    """
    This Spider downloads the list of all documents belonging to the document family
    """

    name = "ted"
    _unavailable: List[str] = []
    _unavailable_path: Path = Path("./unavailable.json")
    _columns = ["id", "award_id", "title", "desc", "criteria", "country_code"]
    _object_title = "Section II:"
    _award_title = "Section V:"
    _val_sec_name = "Total final value of contract(s)"

    def __init__(self):
        super().__init__()
        # read in the config file
        self._parser = configparser.ConfigParser()
        self._parser.read("ted.ini")
        self._config = self._parser["spider"]
        # extract the paths and check that they are valid
        self._input_path = Path(self._config.get("input_file"))
        if not self._input_path.exists():
            print(f"Input path {self._input_path} does not exists.\nExiting")
            sys.exit(1)
        self._unavailable_path = Path(self._config.get("unavailable_path"))
        # read in the data
        self.input_data = pd.read_csv(
            self._input_path,
            dtype={"ID_NOTICE_CAN": str},
        )
        self.dat = pd.DataFrame(columns=self._columns)
        self.input_data = self.input_data[
            self.input_data.ISO_COUNTRY_CODE == self._config.get("country_code")
        ]
        self.writer = OutputFile()
        # get the set of already parsed ids.
        if not self._config.getboolean("reparse", False):
            parsed = self.writer.parsed_ids
            if self._unavailable_path.exists():
                with self._unavailable_path.open() as ifile:
                    self._unavailable = json.load(ifile)
                parsed = parsed.union(set(self._unavailable))
            unavailable_col = self.input_data["ID_NOTICE_CAN"].isin(parsed)
            self.input_data = self.input_data[~unavailable_col]
        self.notice_ids = self.input_data.ID_NOTICE_CAN.unique()

    def close(self, spider, reason):
        self.logger.info("Spider closed method called")
        self.writer.dump()
        with self._unavailable_path.open("w") as ofile:
            json.dump(self._unavailable, ofile, indent=2)
        super().close(spider, reason)

    def start_requests(self):
        """
        Perform one request for each procedure I find in the code.
        """
        for notice_id in self.notice_ids:
            # I need to select all rows which belong to the procedure.
            tmp = self.input_data[self.input_data.ID_NOTICE_CAN == notice_id]
            url = tmp.TED_NOTICE_URL.unique()
            if len(url) != 1:
                self.logger.error(f"Bad url(s) for {notice_id}: {url}")
            else:
                url = url[0]
            yield Request(
                url=f"https://{url}",
                callback=self.parse_TED,
                cb_kwargs={"notice_id": notice_id, "data": tmp},
                meta={"dont_redirect": True, "handle_httpstatus_list": [301, 302]},
            )

    def parse_TED(self, response, notice_id: str, data: pd.DataFrame):
        """
        This function parses the response from TED.
        It extracts the relevant data and writes it to the output dataframe.

        Args:
            response: The response returned by scrapy.
            notice_id (str): The ID of the notice this response belongs to.
            data (pd.DAtaFrame): The data associated with this notice.

        Returns:
            The result is written to the dat dataframe.
            Note, that for one procedure, I can write several rows.
        """
        if response.status in [301, 302]:
            self.logger.info(f"{notice_id} is no longer available")
            self._unavailable.append(notice_id)
            return
        # first extract the sections and their names:
        sections = response.xpath("//div[@class='grseq']")
        # I need to remove sections, which do not have a title.
        to_remove = []
        for i in range(len(sections)):
            if sections[i].xpath("p/text()").get() is None:
                to_remove.append(i)
        for i in sorted(to_remove, reverse=True):
            del sections[i]
        num_sections = len(sections)
        sec_names = sections.xpath("p/text()").extract()
        # now look for the sections I need.
        tmp = [i for i in range(num_sections) if self._object_title in sec_names[i]]
        if len(tmp) == 1:
            sec_object = sections[tmp[0]]
        else:
            self.logger.error(f"Did not find object title in {sec_names}.")
            return
        # now look for all awards
        awards = [i for i in range(num_sections) if self._award_title in sec_names[i]]
        if len(awards) == 0:
            self.logger.error(f"Did not find any awards in {sec_names}.")
            return
        # now I can get the data out of the sections.
        procurement_data: Dict[str, str] = {}
        lot_data: List[Dict[str, str]] = []
        current_lot: Dict[str, str] = {}
        ignore_section = False
        for field in sec_object.xpath("div[@class='mlioccur']"):
            pos = field.xpath("span[@class='nomark']/text()").get()
            # print(pos)
            data = field.xpath("div[@class='txtmark']//text()").extract()
            data = "".join(data).strip()
            # print(data)
            # check if this is general data. If so, append
            if "II.1" in pos and pos != "II.1)":
                procurement_data[pos] = data
            elif "II.2" in pos:
                # this is lot data
                if pos == "II.2)":
                    # a new lot starts here. Append the old (unless empty)
                    if len(current_lot) != 0 and not ignore_section:
                        lot_data.append(current_lot)
                    current_lot = {}
                    # Some old notices have a different section 2, which contains the value.
                    # Therefore, I need to remove this section.
                    ignore_section = field.xpath("span[@class='timark']/text()").get() == self._val_sec_name
                    if ignore_section:
                        self.logger.info(f"detected value section for url {response.url}")
                else:
                    # There is data belonging to the current lot in this field.
                    current_lot[pos] = data
        # and finally check if I need to append the current lot.
        if len(current_lot) != 0 and not ignore_section:
            lot_data.append(current_lot)
        # now I need to transform the lot data into the dataframe
        self.writer.write_notice(notice_id, response.url, procurement_data, lot_data)
