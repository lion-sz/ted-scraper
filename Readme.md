# TED Scraper

This project downloads additional information from TED.

## Output Format

The written csv file has the following fields:

* `id_notice` (str): The notice ID from the original file.
  This also identifies which columns belong together.
* `url` (str): The url to the notice information. I mainly included this since I need it in the viewer script.
  Identical for all rows.
* `has_lots` (bool): Whether the document has any lots. This is taken from a field "II.1.6" in the notice.
  It is however also false, if this field is not present.
  You cannot rely on there being only one row, if this field is false.
* `lot_nr` (int): The lot number parsed from the lot description. If I was unable to parse something, I use the lot position.
  This implies, that there can be several lots with the same lot_nr in the notice.
  The `lot_nr` will be NA for the first row in each notice.
* `lot_pos `(int): The position of the lot in the document. This column is simply counting all rows after the first row (the first row does not have a `lot_pos` assigned).
* `title` (str): The title.
* `desc` (str): The description.


## Proxies

I used proxies for downloading notices in a quicker way.
The proxies are saved in the file `proxy_list.txt`.
While this massively speeds up the crawler, it can also be a problem since e.g. the ZEW does not allow proxies.
There is a flag in the settings to disable proxies.
If you want to use proxies, you have to get your own list (since mine will probably no longer work soon) and change the block in
the settings.py file to work with your proxy file structure.

## Configuration

To configure the scraper, I've set up a config file (`ted.ini`) from which you can adapt the most important settings.
The file is split into three sections, one to configure the spider, another to configure the output file and a final section which is used in the settings.py file.

### spider

* input_file (path): The path where the original file is. I only need the columns `ID_NOTICE_CAN` and `TED_NOTICE_URL`.
* unavailable_path (path): The path where I store the list of unavailable notices. This is stored as a simpel json file.
* country_code (path): The country code for the country to parse.
* reparse (bool, optional, default False): When set, the scraper will not try to skip already parsed notices and instead redo everything.

### output_file

* output_file (path): The path where the output file should be placed. This must be present.

### settings (optional)

* use_proxies (bool, optional, default True): Whether to use proxies.
* log_level (str, optional, default "INFO"): The log level to use. The possible levels can be found [here](https://docs.python.org/3/library/logging.html#logging-levels).
* httpcache_enabled (bool, optional, default no): A boolean indicating whether to use the httpcache.
  If this is set, you must also set the httpcache_dir option.
* httpcache_dir (path, optional): The folder where to save the httpcache.

## httpcache

One very nice feature of scrapy is its build in httpcache.
The way I've set it up (once enabled in the config file), any response is saved and subsequently making the same request will return the saved response.
This is much quicker, than downloading it and therefore, the entire scraper can run from cache in about an hour instead of a day.
So this option is very useful, If you believe you might be interested in other fields later on.
It is probably best, if the `httpcache_dir` is on a fast drive (i.e. SSD), since otherwise the performance will suffer significantly.

## Viewer

I've also written a small flask app, which can be used to view a parsed notice and the original document.
But Since I need to proxy the original document, I loose some of the style information associated with it.
Note that this app is not as customizable as the scraper, some values (e.g. the file from which to read the notices) are hardcoded.
