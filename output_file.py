from pathlib import Path
import pandas as pd
import logging
from typing import Dict, List, Union, Set
import re
import configparser


class OutputFile:
    """
    This class provides the method to dump the output to a file.

    Right now I write 4 columns:
        * id_notice: The id from the input file.
        * lot_nr: The number of the lot within the notice. For the generall information, this is NA.
    """

    _output_path: Path
    _output: List[Dict[str, Union[str, int]]] = []
    _columns: List[str] = [
        "id_notice",
        "url",
        "has_lots",
        "lot_nr",
        "lot_pos",
        "title",
        "desc",
    ]
    _lot_regex = [re.compile("Lot No: ([0-9]+)"), re.compile("Lot No: Los ([0-9]+)")]

    def __init__(self):
        """
        Args:
            output_path: The output file will be written in this path.
        """
        print("Writer opened")
        self._parser = configparser.ConfigParser()
        self._parser.read("ted.ini")
        self._output_path = Path(self._parser.get("output_file", "output_file"))
        self._logger = logging.getLogger("OutputFile")
        # If the output file exists, read this in and get the already processed ids
        if self._output_path.exists():
            self._previous = pd.read_csv(self._output_path, dtype={"id_notice": str})
        else:
            self._previous = None

    @property
    def parsed_ids(self) -> Set[str]:
        """
        The ids of the previously parsed notices.
        If there are None, it returns an empty set.
        """
        if self._previous is not None:
            return set(self._previous.id_notice)
        else:
            return set()

    def dump(self):
        print("Dumping results to file.")
        result = pd.DataFrame.from_dict(self._output)
        result["id_notice"] = result.id_notice.astype(str)
        print(f"Parsed {len(result.id_notice.unique())} notices and added {result.shape[0]} lines.")
        if self._previous is not None:
            result = self._previous.append(result, ignore_index=True)
        if result.shape[0] > 0:
            print(f"Writing {result.shape[0]} lines total.")
            result.to_csv(self._output_path, index=False)

    def write_notice(
        self, id: str, url: str, procurement: Dict[str, str], lots: List[Dict[str, str]]
    ):
        """
        This function writes the notice data to the output file.

        If I do not find the title or description of the procurement,
        I do not write anything and log an error.
        If I'm missing some data for a specific lot, I will continue writing,
        but log a warning.

        Args:
            id: The ID_NOTICE_CAN from the original file.
            procurement: The procurement data created in the spider.
            lots: The notice data object created in the spider.
        """
        # first write the procurement data, lot id is None
        if "II.1.1)" not in procurement:
            self._logger.error(f"For {id} no procurement title was found")
            return
        if "II.1.4)" not in procurement:
            self._logger.error(f"For {id} no procurement description was found")
            return
        # check lot information
        has_lots = "yes" in procurement.get("II.1.6)", "")
        proc = {
            "id_notice": id,
            "url": url,
            "has_lots": has_lots,
            "title": procurement["II.1.1)"],
            "desc": procurement["II.1.4)"],
        }
        self._output.append(proc)
        # now iterate through the lots and append those
        for i in range(len(lots)):
            lot = lots[i]
            lot_nr = i + 1
            if "II.2.1)" not in lot:
                self._logger.warning(f"For {id} no lot title was found.")
                title = pd.NA
            else:
                title = lot["II.2.1)"]
                for regex in self._lot_regex:
                    if regex.search(title):
                        lot_nr = regex.search(title).groups()[0]
                        break
            if "II.2.4)" not in lot:
                self._logger.warning(f"For {id} no lot description was found.")
                desc = pd.NA
            else:
                desc = lot["II.2.4)"]
            row = {
                "id_notice": id,
                "url": url,
                "has_lots": has_lots,
                "lot_nr": lot_nr,
                "lot_pos": i + 1,
                "title": title,
                "desc": desc,
            }
            self._output.append(row)
