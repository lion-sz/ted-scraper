from lxml import etree
from pathlib import Path
from typing import List, Dict, Optional
import re


class DocumentParser:
    """
    This class reads in a document and stores
    all relevant information from it.

    I extract the following elements:
        * doc_id:
        * uri[lan]: link to the publication on ted.europa.eu, in the different languages
    """
    #_etendering_regex = re.compile(r"https://etendering.ted.europa.eu/cft/cft-display.html\?cftId=(\d{4})")
    _etendering_regex = re.compile("europa.eu/cft/cft-display.html\?cftId=(\d{4})")

    _path: Path
    _doc: etree.ElementTree
    _root: etree.Element
    _ns: Dict

    doc_id: str
    cftId: Optional[str] = None
    uri: Dict[str, str] = {}

    def __init__(self, path: Path):
        """
        To create a document, I require the path to the document
        """
        self._path = path
        self._doc = etree.parse(str(self._path))
        self._root = self._doc.getroot()
        self._ns = self._root.nsmap

        self.parse()
        self.search_etendering_uri()

    @property
    def etendering_uri(self) -> str:
        """
        Returns the etendering uri. None if there is no uri.
        """
        if self.cftId is None:
            return None
        else:
            return f"https://etendering.ted.europa.eu/cft/cft-display.html?cftId={self.cftId}"

    def parse(self):
        """
        """
        self.doc_id = self._root.get("DOC_ID")
        # get the ojs ids
        self.doc_ojs = self._doc.find("CODED_DATA_SECTION/NOTICE_DATA/NO_DOC_OJS", self._ns).text
        if self._doc.find("CODED_DATA_SECTION/NOTICE_DATA/REF_NOTICE", self._ns):
            self.ref_ojs = self._doc.find("CODED_DATA_SECTION/NOTICE_DATA/REF_NOTICE", self._ns).text
        # and get all the ids.
        uri_list = self._doc.find("CODED_DATA_SECTION/NOTICE_DATA/URI_LIST/URI_DOC", self._ns)
        for uri in uri_list:
            self.uri[uri.get("LG")] = uri.text

    def search_etendering_uri(self):
        """
        This function searchs for a uri linking to a website on
        "etendering.ted.europa.eu". If I find this directly in the document,
        it can save me a couple calls to the ted website.
        """
        parts = self._doc.findall("//P", self._ns)
        res = []
        for elem in parts:
            if elem.text and "etendering" in elem.text:
                res.append(elem.text)
        for part in res:
            tmp = re.search(self._etendering_regex, part)
            if tmp:
                self.cftId = tmp.groups()[0]
