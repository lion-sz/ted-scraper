from pathlib import Path

from document_parser import DocumentParser


has = 0
has_nots = 0
input_path = Path("/data/TED/")
for path in input_path.glob("*"):
    if not path.is_dir():
        continue
    for file in path.glob("*"):
        doc = DocumentParser(file)

        if doc.etendering_uri is not None:
            has += 1
        else:
            has_nots += 1

print(f"{has} have and {has_nots} have not")
