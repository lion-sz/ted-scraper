from flask import Flask, render_template, make_response, send_file
import pandas as pd
import requests
import aiohttp
import random


app = Flask(__name__, static_folder=None)

processed = pd.read_csv("../results.csv.zip", dtype={"id_notice": str})
#processed = pd.read_csv("../small_results.csv", dtype={"id_notice": str})

ids = processed.id_notice.unique()[1:]
random.shuffle(ids)

current_pos = -1
url = None

@app.route("/")
def render_next():
    global current_pos
    global url
    current_pos +=1
    if current_pos >= len(ids):
        return "Out of notices"
    notice_id = ids[current_pos]
    data = processed[processed.id_notice == notice_id]
    print(data.shape)
    url = data.url.iloc[0]
    response = render_template("index.html", notice_id=notice_id, data=data, url=url)
    response = make_response(response)
    response.headers.set("Access-Control-Allow-Origin", "*")
    return response

@app.route("/source")
def get_source():
    res = requests.get(url)
    return res.text

static_responses = {}

@app.route("/static/<path:filename>")
async def static(filename):
    if filename == "index.css":
        return send_file("static/index.css")
    if filename in static_responses:
        res = static_responses[filename]
    else:
        async with aiohttp.ClientSession() as session:
            async with session.get(f"https://ted.europa.eu/static/{filename}") as response:
                res = await response.read()
            static_responses[filename] = res
    return res

if __name__ == "__main__":
    app.run()